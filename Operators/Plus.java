package Operators;

public class Plus {

	public static void main(String[] args) {
		byte b=12;
		b++;
		//b=b+1; will give you compilation error coz + output is int; so downcasting is requried
		b=(byte)(b+1);
		System.out.println(b);
		
	}
}
