package arrays;

public class SmallestInArray {

	public static void main(String[] args) {
		int array[] = {2,6,-1,0,4,99,66,1};
		smallestInArray(array);
	}
	private static void smallestInArray(int[] array) {
		int smallest =array[0];int index=0;
		for (int i = 0; i < array.length; i++) {
	     	if(smallest > array[i]) {smallest = array[i];	index = i; }}
		System.out.println("Smallest no is "+smallest + " at index "+ index);
	}
}
